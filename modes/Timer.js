/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Timer(gui, length)
{
    Amarok.debug("-> " + arguments.callee.name);

    this.mode = new Mode(gui);
    this.endtime = new Date(new Date().getTime() + length);
    this.timer = new QTimer();
    this.timer.timeout.connect(this, this.timeout);

    Amarok.debug("Mode: timer [to " + this.endtime.toTimeString() + "]");
    Amarok.debug("<- " + arguments.callee.name);
}

Timer.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Timer." + "connect");

    this.mode.connect(receiver, member);
    this.timer.start(0);

    Amarok.debug("<- Timer." + "connect");
}

Timer.prototype.disconnect = function()
{
    Amarok.debug("-> Timer." + "disconnect");

    this.mode.disconnect();
    this.timer.stop();

    Amarok.debug("<- Timer." + "disconnect");
}

Timer.prototype.timeout = function()
{
    Amarok.debug("-> Timer." + "timeout");

    var time_left = this.endtime.getTime() - new Date().getTime();
    var minutes_left = Math.ceil(time_left / 1000 / 60);

   this.mode.setStatus("Less than " + minutes_left + " minutes left...");

    if (time_left <= 0) {
	this.timer.stop();
	this.mode.signal();
	return;
    }

    if (time_left <= 60 * 1000) {
	this.timer.interval = time_left;
    } else {
	this.timer.interval = 60 * 1000;
    }

    Amarok.debug("<- Timer." + "timeout");
}
