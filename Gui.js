/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Gui()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Load ui file
    var uiLoader = new QUiLoader(this);
    var uiFile = new QFile(Amarok.Info.scriptPath() + "/Gui.ui");
    uiFile.open(QIODevice.ReadOnly);
    this.dialog = uiLoader.load(uiFile, this);

    Amarok.debug("<- " + arguments.callee.name);
}

Gui.prototype.setValues = function(config)
{
    Amarok.debug("-> Gui." + "setValues");

    var gui, conf;

    // Mode
    gui = this.dialog.modeGroupBox;
    conf = config.mode;

    gui.musicRadioButton.setChecked(conf.music.enabled);
    gui.albumRadioButton.setChecked(conf.album.enabled);
    gui.songsRadioButton.setChecked(conf.songs.enabled);
    gui.songsSpinBox.setValue(conf.songs.value);
    gui.timeoutRadioButton.setChecked(conf.timeout.enabled);
    gui.timeoutSpinBox.setValue(conf.timeout.value);
    gui.timeRadioButton.setChecked(conf.time.enabled);
    gui.timeTimeEdit.setTime(conf.time.time);

    // Fade-out
    gui = this.dialog.fadeoutGroupBox;
    conf = config.fadeout;

    gui.durationCheckBox.setChecked(conf.enabled);
    gui.durationSpinBox.setValue(conf.value);

    // Sleep
    gui = this.dialog.sleepGroupBox;
    conf = config.sleep;

    gui.turnoffCheckBox.setChecked(conf.turnoff.enabled);
    gui.quitCheckBox.setChecked(conf.quit.enabled);
    gui.energyCheckBox.setChecked(conf.energy.enabled);
    gui.energyComboBox.setCurrentIndex(conf.energy.index);

    Amarok.debug("<- Gui." + "setValues");
}

Gui.prototype.getValues = function(config)
{
    Amarok.debug("-> Gui." + "getValues");

    var conf, gui;

    // Mode
    conf = config.mode;
    gui  = this.dialog.modeGroupBox;

    conf.music.enabled   = gui.musicRadioButton.checked;
    conf.album.enabled   = gui.albumRadioButton.checked;
    conf.songs.enabled   = gui.songsRadioButton.checked;
    conf.songs.value     = gui.songsSpinBox.value;
    conf.timeout.enabled = gui.timeoutRadioButton.checked;
    conf.timeout.value   = gui.timeoutSpinBox.value;
    conf.time.enabled    = gui.timeRadioButton.checked;
    conf.time.time       = gui.timeTimeEdit.time;

    // Fade-out
    conf = config.fadeout;
    gui  = this.dialog.fadeoutGroupBox;

    conf.enabled = gui.durationCheckBox.checked;
    conf.value   = gui.durationSpinBox.value;

    // Sleep
    conf = config.sleep;
    gui  = this.dialog.sleepGroupBox;

    conf.turnoff.enabled = gui.turnoffCheckBox.checked;
    conf.quit.enabled    = gui.quitCheckBox.checked;
    conf.energy.enabled  = gui.energyCheckBox.checked;
    conf.energy.index    = gui.energyComboBox.currentIndex;

    Amarok.debug("<- Gui." + "getValues");
}

Gui.prototype.setModeStatus = function(message)
{
    Amarok.debug("-> Gui." + "setModeStatus");

    var label = this.dialog.actionGroupBox.modeStatusLabel;

    label.setText(message);
    label.setStyleSheet("QLabel { color : darkRed; }");

    Amarok.debug("<- Gui." + "setModeStatus");
}

Gui.prototype.setPmStatus = function(suspend, hibernate)
{
    Amarok.debug("-> Gui." + "setPmStatus");

    var label = this.dialog.sleepGroupBox.pmStatusLabel;

    if (suspend == false && hibernate == false) {
	label.setText("Suspend to RAM/disk are not available !");
    } else if (suspend == false) {
	label.setText("Suspend to RAM is not available !");
    } else if (hibernate == false) {
	label.setText("Suspend to disk is not available !");
    } else {
	label.setText("");
	//label.setText("Everything alright buddy.");
    }

    Amarok.debug("<- Gui." + "setPmStatus");
}

Gui.prototype.show = function(enabled)
{
    Amarok.debug("-> Gui." + "show");

    var label = this.dialog.actionGroupBox.modeStatusLabel;

    if (enabled == true) {
	// Status label is set by setModeStatus, we don't have
	// to care about it anymore.
    } else {
	label.setText("Disabled");
        label.setStyleSheet("QLabel { color : darkGreen; }");
    }

    this.dialog.show();

    Amarok.debug("<- Gui." + "show");
}
