/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function MusicOver(gui)
{
    Amarok.debug("-> " + arguments.callee.name);

    this.mode = new Mode(gui);

    Amarok.debug("Mode: musicover");
    Amarok.debug("<- " + arguments.callee.name);
}

MusicOver.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> MusicOver." + "connect");

    this.mode.connect(receiver, member);
    Amarok.Engine.trackFinished.connect(this, this.callback);

    Amarok.debug("<- MusicOver." + "connect");
}

MusicOver.prototype.disconnect = function()
{
    Amarok.debug("-> MusicOver." + "disconnect");

    this.mode.disconnect();
    Amarok.Engine.trackFinished.disconnect(this, this.callback);

    Amarok.debug("<- MusicOver." + "disconnect");
}

MusicOver.prototype.callback = function()
{
    Amarok.debug("-> MusicOver." + "callback");

    this.mode.signal();

    Amarok.debug("<- MusicOver." + "callback");
}