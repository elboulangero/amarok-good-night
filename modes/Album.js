/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Album(gui, title)
{
    Amarok.debug("-> " + arguments.callee.name);

    this.mode = new Mode(gui);
    this.title = title;

    Amarok.debug("Mode: album [" + this.title + "]");
    Amarok.debug("<- " + arguments.callee.name);
}

Album.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Album." + "connect");

    this.mode.connect(receiver, member);
    Amarok.Engine.trackChanged.connect(this, this.callback);

    Amarok.debug("<- Album." + "connect");
}

Album.prototype.disconnect = function()
{
    Amarok.debug("-> Album." + "disconnect");

    this.mode.disconnect();
    Amarok.Engine.trackChanged.disconnect(this, this.callback);

    Amarok.debug("<- Album." + "disconnect");
}

Album.prototype.callback = function ()
{
    Amarok.debug("-> Album." + "callback");

    var title = Amarok.Engine.currentTrack().album;

    if (this.title == "") {
	this.title = title;
	Amarok.debug("Mode album -> [" + this.title + "]");
    } else {
	if (this.title != title) {
	    this.mode.signal();
	}
    }

    Amarok.debug("<- Album." + "callback");
}