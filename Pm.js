/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Pm(gui)
{
    Amarok.debug("-> " + arguments.callee.name);

    this.gui = gui;
    this.canSuspend = undefined;
    this.canHibernate = undefined;

    Amarok.debug("<- " + arguments.callee.name);
}

Pm.prototype.check = function()
{
    Amarok.debug("-> Pm." + "check");

    var canSuspendProcess = new QProcess();
    canSuspendProcess['finished(int,QProcess::ExitStatus)'].connect(this, this.canSuspendCallback);
    canSuspendProcess.start(Amarok.Info.scriptPath() + "/pm-scripts/can-suspend.sh");
    var canHibernateProcess = new QProcess();
    canHibernateProcess['finished(int,QProcess::ExitStatus)'].connect(this, this.canHibernateCallback);
    canHibernateProcess.start(Amarok.Info.scriptPath() + "/pm-scripts/can-hibernate.sh");

    Amarok.debug("<- Pm." + "check");
}

Pm.prototype.canSuspendCallback = function (exitcode, exitstatus)
{
    Amarok.debug("-> Pm." + "canSuspendCallback");

    this.canSuspend = exitcode == 0 ? true : false;
    if (this.canHibernate != undefined) {
	this.gui.setPmStatus(this.canSuspend, this.canHibernate);
    }

    Amarok.debug("<- Pm." + "canSuspendCallback");
}

Pm.prototype.canHibernateCallback = function (exitcode, exitstatus)
{
    Amarok.debug("-> Pm." + "canHibernateCallback");

    this.canHibernate = exitcode == 0 ? true : false;
    if (this.canSuspend != undefined) {
	this.gui.setPmStatus(this.canSuspend, this.canHibernate);
    }

    Amarok.debug("<- Pm." + "canHibernateCallback");
}
