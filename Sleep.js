/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

Importer.include("Fadeout.js");
Importer.include("modes/Mode.js");
Importer.include("modes/MusicOver.js");
Importer.include("modes/Album.js");
Importer.include("modes/Songs.js");
Importer.include("modes/Timer.js");

function Sleep(gui)
{
    Amarok.debug("-> " + arguments.callee.name);

    // Gui
    this.gui = gui;

    // Receiver
    this.receiver = null;
    this.member = null;

    // Mode
    this.mode = null;

    // Fade-out
    this.fader = new Fadeout();
    this.faderLength = -1;
    this.faderInitVolume = -1;

    // Fader starter
    this.faderStarter = new QTimer();
    this.faderStarter.singleShot = true;
    this.faderStarter.timeout.connect(this, this.faderStarterTimeout);
    this.faderStarterLength = -1;

    Amarok.debug("<- " + arguments.callee.name);
}

Sleep.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Sleep." + "connect");

    this.receiver = receiver;
    this.member = member;

    Amarok.debug("<- Sleep." + "connect");
}

Sleep.prototype.start = function(config)
{
    Amarok.debug("-> Sleep." + "start");

    // Stop and init some values
    this.stop();
    this.faderLength = -1;
    this.faderInitVolume = -1;
    this.faderStarterLength = -1

    // What to do ?
    if (config.mode.music.enabled == true) {
	this.mode = new MusicOver(this.gui);
    } else if (config.mode.album.enabled == true) {
	var album;
	if (Amarok.Engine.engineState() == 0) {
	    album = Amarok.Engine.currentTrack().album;
	} else {
	    album = "";
	}
	this.mode = new Album(this.gui, album);
    } else if (config.mode.songs.enabled == true) {
	var songs_left = config.mode.songs.value;
	this.mode = new Songs(this.gui, songs_left);
    } else {
	var msecs_to_sleep;
	var secs_to_sleep;

	if (config.mode.timeout.enabled == true) {
	    secs_to_sleep = config.mode.timeout.value * 60;
	    msecs_to_sleep = secs_to_sleep * 1000;
	} else if (config.mode.time.enabled == true) {
	    var now_date = new Date();
	    var sleep_date = new Date(now_date.getTime());
	    sleep_date.setHours(config.mode.time.time.hour());
	    sleep_date.setMinutes(config.mode.time.time.minute());
	    sleep_date.setSeconds(0);
	    sleep_date.setMilliseconds(0);
	    if (sleep_date < now_date) {
		sleep_date.setDate(sleep_date.getDate() + 1);
	    }
	    msecs_to_sleep = sleep_date.getTime() - now_date.getTime();
	    secs_to_sleep = Math.floor(msecs_to_sleep / 1000);
	} else {
	    Amarok.debug("What the hell is going on ?");
	    return;
	}

	this.mode = new Timer(this.gui, msecs_to_sleep);

	if (config.fadeout.enabled == true) {
	    var fadeout_length = Math.min(config.fadeout.value * 60, secs_to_sleep);
	    var secs_to_fadeout = secs_to_sleep - fadeout_length;
	    var msecs_to_fadeout = secs_to_fadeout * 1000;

	    this.faderLength = fadeout_length;
	    this.faderStarterLength = msecs_to_fadeout;
	    Amarok.debug("Fade-out in " + secs_to_fadeout + " secs, for " +
			 fadeout_length + " secs");
	}
    }

    // Start mode
    this.mode.connect(this.receiver, this.member);

    // Fader starter
    if (this.faderStarterLength != -1) {
	this.faderStarter.start(this.faderStarterLength);
    }

    // Fader is started in faderStarterTimeout

    Amarok.debug("<- Sleep." + "start");
}

Sleep.prototype.stop = function()
{
    Amarok.debug("-> Sleep." + "stop");

    // Fader
    this.fader.stop();

    // Fader starter
    this.faderStarter.stop();

    // Stop any running mode
    if (this.mode != null) {
	this.mode.disconnect();
	this.mode = null;
    }

    Amarok.debug("<- Sleep." + "stop");
}

Sleep.prototype.isActive = function()
{
    Amarok.debug("-> Sleep." + "isActive");

    return (this.mode != null);

    Amarok.debug("<- Sleep." + "isActive");
}

Sleep.prototype.action = function()
{
    Amarok.debug("-> Sleep." + "action at " + new Date());

    // Stop ourselves
    this.stop();

    // Stop Amarok
    Amarok.Engine.Stop(true);

    // Restore volume if fade-out happened
    if (this.faderInitVolume != -1) {
	Amarok.Engine.volume = this.faderInitVolume;
    }

    Amarok.debug("<- Sleep." + "action");
}

// Private methods

Sleep.prototype.faderStarterTimeout = function()
{
    Amarok.debug("-> Sleep." + "faderStarterTimeout");

    // Save initial volume before starting fade-out
    this.faderInitVolume = Amarok.Engine.volume;
    this.fader.start(this.faderLength);

    Amarok.debug("<- Sleep." + "faderStarterTimeout");
}
