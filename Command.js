/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Command()
{
    Amarok.debug("-> " + arguments.callee.name);

    Amarok.debug("<- " + arguments.callee.name);
}

Command.process = function(cmd)
{
    Amarok.debug("-> Command." + "process");

    var process = new QProcess();
    process.start(cmd);
    if (!process.waitForFinished(-1)) {
	Amarok.debug(cmd + ": failed to execute.");
	return -1;
    }
    if (process.exitCode() != 0) {
	Amarok.debug(cmd + ": returned " + process.exitCode());
	return -1;
    }

    Amarok.debug("<- Command." + "process");
}

Command.screenlock = function()
{
    Amarok.debug("-> Command." + "screenlock");

    var cmd = "qdbus " +
	"org.freedesktop.ScreenSaver " +
	"/ScreenSaver " +
	"org.freedesktop.ScreenSaver.Lock";
    Command.process(cmd);

    Amarok.debug("<- Command." + "screenlock");
}

Command.suspend = function()
{
    Amarok.debug("-> Command." + "suspend");

    var cmd = "qdbus " +
	"org.freedesktop.PowerManagement " +
	"/org/freedesktop/PowerManagement " +
	"org.freedesktop.PowerManagement.Suspend";
    Command.process(cmd);

    Amarok.debug("<- Command." + "suspend");
}

Command.hibernate = function()
{
    Amarok.debug("-> Command." + "hibernate");

    var cmd = "qdbus " +
	"org.freedesktop.PowerManagement " +
	"/org/freedesktop/PowerManagement " +
	"org.freedesktop.PowerManagement.Hibernate";
    Command.process(cmd);

    Amarok.debug("<- Command." + "hibernate");
}

Command.shutdown = function()
{
    Amarok.debug("-> Command." + "shutdown");

    var cmd = "qdbus " +
	"org.kde.ksmserver " +
	"/KSMServer " +
	"org.kde.KSMServerInterface.logout " +
	"1 2 2";
    Command.process(cmd);

    Amarok.debug("<- Command." + "shutdown");
}