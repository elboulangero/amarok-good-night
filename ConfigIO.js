/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

/*
 * capitaliseFirstLetter() from
 * http://stackoverflow.com/questions/1026069/capitalize-the-first-letter-of-string-in-javascript
 */

function ConfigIO()
{
    Amarok.debug("-> " + arguments.callee.name);

    Amarok.debug("<- " + arguments.callee.name);
}

ConfigIO.read = function(config)
{
    Amarok.debug("-> ConfigIO." + "read");

    configDeserialize(config, "");

    Amarok.debug("<- ConfigIO." + "read");
}

ConfigIO.write = function(config)
{
    Amarok.debug("-> ConfigIO." + "write");

    configSerialize(config, "");

    Amarok.debug("<- ConfigIO." + "write");
}

// Private functions

function parseBoolean (string)
{
    switch (string) {
    case "true":
	return true;
    case "false":
    default:
	return false;
    }
}

function configDeserialize(obj, prefix)
{
    for (var key in obj) {
	var val = obj[key];
	var def = val.toString();
	var confkey = prefix + capitaliseFirstLetter(key);
	if (typeof val == "object") {
	    if (val instanceof QTime) {
		obj[key] = QTime.fromString(Amarok.Script.readConfig(confkey, def));
		//Amarok.debug("T: " + key + ": " + val.toString());
	    } else {
		configDeserialize(val, confkey);
		//Amarok.debug("O: " + key);
	    }
	} else if (typeof val == "boolean") {
	    obj[key] = parseBoolean(Amarok.Script.readConfig(confkey, def));
	    //Amarok.debug("B: " + key + ": " + val.toString());
	} else if (typeof val == "number") {
	    obj[key] = parseInt(Amarok.Script.readConfig(confkey, def));
	    //Amarok.debug("N: " + key + ": " + val.toString());
	} else { // string
	    obj[key] = Amarok.Script.readConfig(confkey, def);
	    //Amarok.debug("S: " + key + ": " + val.toString());
	}
    }
}

function capitaliseFirstLetter(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function configSerialize(obj, prefix)
{
    for (var key in obj) {
	var val = obj[key];
	var confkey = prefix + capitaliseFirstLetter(key);
	if (typeof val == "object") {
	    if (val instanceof QTime) {
		Amarok.Script.writeConfig(confkey, val.toString());
	    } else {
		configSerialize(val, confkey);
	    }
	} else { // boolean, number, string
	    Amarok.Script.writeConfig(confkey, val.toString());
	}
    }
}

