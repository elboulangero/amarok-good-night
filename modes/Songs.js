/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Songs(gui, counter)
{
    Amarok.debug("-> " + arguments.callee.name);

    this.mode = new Mode(gui);
    this.counter = counter;

    Amarok.debug("Mode: songs [" + this.counter + "]");
    Amarok.debug("<- " + arguments.callee.name);
}

Songs.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Songs." + "connect");

    this.mode.connect(receiver, member);
    Amarok.Engine.trackChanged.connect(this, this.callback);

    Amarok.debug("<- Songs." + "connect");
}

Songs.prototype.disconnect = function()
{
    Amarok.debug("-> Songs." + "disconnect");

    this.mode.disconnect();
    Amarok.Engine.trackChanged.disconnect(this, this.callback);

    Amarok.debug("<- Songs." + "disconnect");
}

Songs.prototype.callback = function()
{
    Amarok.debug("-> Songs." + "callback");
    Amarok.debug("Mode songs: " + this.counter + " left");

    this.mode.setStatus(this.counter + " songs left...");

    if (this.counter-- == 0) {
	this.mode.signal();
    }

    Amarok.debug("<- Songs." + "callback");
}